function validate(req,next){
    let { directory, files } = req.body;
    if (req.headers['content-type'] !== 'application/json') {
        next({
            isCustomError: true,
            statusCode: 415,
            message: `please provide data in json format`
        });
    }
    
    else if (directory === undefined || files === undefined) {
        next({
            isCustomError: true,
            statusCode: 412,
            message: "Invalid body content"
        });
    }
    
    else if (typeof directory !== 'string' || directory === "" || !(Array.isArray(files))) {
        next({
            isCustomError: true,
            statusCode: 412,
            message: `please provide both directory name and file names`
        });
    }
    else {
        files = files.filter((fileName) => {
            return fileName;
        });
        
        let names = [directory, ...files];
        names = names.map((name)=>{
            if(typeof name === 'string'){
                return name.trim();
            }else{
                return name;
            }
        })
        
        let nonStringFileName = names.filter((name) => {
            return typeof name !== "string";
        });
        if (nonStringFileName.length) {
            next({
                isCustomError: true,
                statusCode: 415,
                message: `Invalid file or directory name`
            });
        }
        
        else {
            let wrongFileName = names.filter((name) => {
                return name.includes('/');
            });
            
            if (wrongFileName.length) {
                next({
                    isCustomError: true,
                    statusCode: 415,
                    message: `Invalid file or directory name`
                });
            }

            else if (!files.length) {
                next({
                    isCustomError: true,
                    statusCode: 415,
                    message: `filenames must be non empty`
                });
            }

            else{
                return true;
            }
        }
    }
}
module.exports = validate;