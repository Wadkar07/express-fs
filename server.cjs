const express = require('express');
const app = express();
const errorHandler = require('./middleware/errorHandler.cjs');

const createRandomDirectoriesAndFiles = require('./routes/createRandomDirectoryAndFiles.cjs');
const deleteFiles = require('./routes/deleteFiles.cjs');

app.use('/createfiles', createRandomDirectoriesAndFiles);
app.use('/deletefiles', deleteFiles);

app.use((err, req, res, next) => {
    errorHandler(err, req, res, next);
});

app.all('/**', (req, res) => {
    res.status(404).json({ error: `page not found` });
});

app.listen(8000, () => {
    console.log('Listening on port 8000');
});