const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
router.use(express.json());

const validateInput = require('../util/validateInput.cjs');

function access(body) {
    return new Promise((resolve, reject) => {
        fs.access(body.directory, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            else {
                resolve(body.files);
            }
        });
    });
}

function deleteFiles(filenames, deletedFiles, notFoundFiles) {
    let promises = filenames.map((file) => {
        return new Promise((resolve, reject) => {
            fs.unlink(file + '.json', (err) => {
                if (err) {
                    if (notFoundFiles["files no found"]) {
                        notFoundFiles["files no found"].push(file);
                    }

                    else {
                        notFoundFiles["files no found"] = [];
                        notFoundFiles["files no found"].push(file);
                    }
                    reject(err);
                }
                else {
                    if (deletedFiles["files deleted"]) {
                        deletedFiles["files deleted"].push(file);
                    }

                    else {
                        deletedFiles["files deleted"] = [];
                        deletedFiles["files deleted"].push(file);
                    }

                    resolve(file);
                }
            });
        });
    });
    return Promise.allSettled(promises);
}



router.delete('/', (req, res, next) => {
    let { directory, files } = req.body;
    if (validateInput(req, next)) {
        let deletedFiles = {};
        let notFoundFiles = {};
        access(req.body)
            .then(() => {
                const filenames = files.map((file) => {
                    return path.join(directory, file);
                });
                return deleteFiles(filenames, deletedFiles, notFoundFiles);
            }).then((data) => {
                let status = 500;
                if (notFoundFiles["files no found"]) {
                    status = notFoundFiles["files no found"].length === files.length ? 404 : 206;
                }
                else {
                    status = 200;
                }
                let returnObject = {
                    directory: directory,
                    ...deletedFiles,
                    ...notFoundFiles
                }
                res.status(status)
                    .json(returnObject).end();
            })
            .catch((err) => {
                next(err);
            });
    } else {
        next(err);
    }
});

module.exports = router;