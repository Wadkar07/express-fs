const express = require('express');
const router = express.Router();

const fs = require('fs');
const path = require('path');

const validateInput = require('../util/validateInput.cjs');

router.use(express.json());


function mkdir(body) {
    return new Promise((resolve, reject) => {
        fs.mkdir(body, { recursive: true }, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            else {
                resolve(body);
            }
        });
    });
}

function createFiles(directoryName, files) {
    let promises = files.map((filename) => {
        let filePath = path.join(directoryName, filename + '.json');
        return new Promise((resolve, reject) => {
            fs.writeFile(filePath, JSON.stringify({ filename }, null, 4), (err) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    resolve(filename);
                }
            });
        });

    });
    return Promise.all(promises);
}

router.post('/', (req, res, next) => {
    let { directory, files } = req.body;
    if (validateInput(req,next)) {
        mkdir(directory)
            .then((directoryName) => {
                return createFiles(directoryName, files);
            }).then((data) => {
                res.status(201)
                    .json({
                        directory: directory,
                        filesCreated: files,
                        message: 'files created successfully'
                    })
                    .end();
            }).catch((err) => {
                next(err);
            });
    }else{
        next(err);
    }

});

module.exports = router;