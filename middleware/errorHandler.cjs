function errorHandler(err, req, res, next){
    if (err.code === 'ENOENT') {
        res.status(206).json({
            directory: req.body.directory,
            error: `${err.path.split('/')[1]} doesn't exist in  ${req.body.directory}`,
            status: "deletion unsuccessful"
        }).end();
    }
    
    else if (err.code === 'EACCES') {
        res.status(500)
            .json({
                status: 'Internal Server Error'
            }).end();
    }

    else if (err.isCustomError) {
        res.status(err.statusCode)
            .json({
                error: err.message
            }).end();
    }
    
    else {
        res.status(500).json({
            error: "something Broke"
        }).end()
    }
}
module.exports = errorHandler;